const { test, expect } = require('@jest/globals')
const func = require('./acm')

test('standard test case', () => {
  expect(func(['10101', '11110', '00010'])).toEqual([5, 1])
})

test('invalid input', () => {
  expect(() => {
    func(null)
  }).toThrowError('Invalid')
})
