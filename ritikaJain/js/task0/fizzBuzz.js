const fizzBuzz = (n) => {
  if (isNaN(n) || n <= 0) {
    throw new Error('Invalid Input')
  }
  const ans = []
  for (let i = 1; i <= n; i++) {
    let x = false
    if (i % 15 === 0) {
      x = 'FizzBuzz'
    } else if (i % 3 === 0) {
      x = 'Fizz'
    } else if (i % 5 === 0) {
      x = 'Buzz'
    }
    if (!x) ans.push(i.toString())
    if (x) ans.push(x)
  }
  console.log(ans)
  return ans
}

module.exports = fizzBuzz
