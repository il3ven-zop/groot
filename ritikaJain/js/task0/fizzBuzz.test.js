const { test, expect } = require('@jest/globals')
const func = require('./fizzBuzz')

test('should pass for n=15', () => {
  expect(func(15)).toEqual([
    '1',
    '2',
    'Fizz',
    '4',
    'Buzz',
    'Fizz',
    '7',
    '8',
    'Fizz',
    'Buzz',
    '11',
    'Fizz',
    '13',
    '14',
    'FizzBuzz'
  ])
})

test('should throw an error for negative integer', () => {
  expect(() => {
    func(-1)
  }).toThrowError('Invalid Input')
})

test('should throw an error if not a number', () => {
  expect(() => {
    func()
  }).toThrowError('Invalid Input')
})

test('should pass for n=3', () => {
  expect(func(3)).toEqual(['1', '2', 'Fizz'])
})

test('should pass for n=5', () => {
  expect(func(5)).toEqual(['1', '2', 'Fizz', '4', 'Buzz'])
})
