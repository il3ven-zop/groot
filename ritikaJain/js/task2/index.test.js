const fs = require('fs')
const axios = require('axios')

const { readFile, writeFile, getUrl, getAllUrl, main } = require('./')

beforeEach(() => jest.restoreAllMocks())

describe('Spy on writeFile', () => {
  test('should accept if file generated and written', () => {
    const spy = jest
      .spyOn(fs, 'writeFile')
      .mockImplementation((_, __, callback) => {
        callback()
      })

    return writeFile('https://dog.ceo/api/breed/hound/images/random').then(
      () => {
        expect(spy).toHaveBeenCalledTimes(1)
      }
    )
  })

  test('should reject if file not generated', () => {
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, callback) => {
      callback(new Error('Something went wrong'))
    })

    return expect(() =>
      writeFile('https://dog.ceo/api/breed/hound/images/random')
    ).rejects.toMatchSnapshot()
  })
})

describe('Spy on readFile', () => {
  test('should accept if file read successfully', () => {
    const spy = jest
      .spyOn(fs, 'readFile')
      .mockImplementation((_, __, callback) => {
        callback()
      })

    return readFile().then(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  test('should reject if file not read', () => {
    const spy = jest
      .spyOn(fs, 'readFile')
      .mockImplementation((_, __, callback) => {
        callback(new Error('something went wrong'))
      })

    return readFile().catch(() => {
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})

describe('Spy on getAllUrl for fetching 5 random images', () => {
  test(`should return 12345`, () => {
    jest
      .spyOn(axios, 'get')
      .mockImplementation(() => Promise.resolve({ data: { message: '12345' } }))

    return getAllUrl('hound').then((data) => {
      expect(data).toEqual(['12345', '12345', '12345', '12345', '12345'])
    })
  })

  test('should throw an error if not fetched', () => {
    const spy = jest
      .spyOn(axios, 'get')
      // eslint-disable-next-line prefer-promise-reject-errors
      .mockImplementation(() => Promise.reject('error'))

    return getAllUrl('hound').catch((err) => {
      expect(err).toBe('error')
      expect(spy).toHaveBeenCalledTimes(5)
    })
  })
})

describe('Spy on axios get', () => {
  test(`should return 12345 if get works well`, () => {
    const spy = jest
      .spyOn(axios, 'get')
      .mockImplementation(() => Promise.resolve({ data: { message: '12345' } }))

    return getUrl('hound').then((data) => {
      expect(data).toBe('12345')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })

  test('should throw an error', () => {
    const spy = jest
      .spyOn(axios, 'get')
      // eslint-disable-next-line prefer-promise-reject-errors
      .mockImplementation(() => Promise.reject('error'))

    return getUrl('hound').catch((err) => {
      expect(err).toBe('error')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
})

describe('Spy on the main function', () => {
  test('check that main function works', () => {
    const spyGet = jest
      .spyOn(axios, 'get')
      .mockImplementation(() => Promise.resolve({ data: { message: '12345' } }))
    const spyRead = jest
      .spyOn(fs, 'readFile')
      .mockImplementation((_, __, callback) => {
        callback()
      })
    const spyWrite = jest
      .spyOn(fs, 'writeFile')
      .mockImplementation((_, __, callback) => {
        callback()
      })
    return main().then(() => {
      expect(spyRead).toHaveBeenCalledTimes(1)
      expect(spyGet).toHaveBeenCalledTimes(5)
      expect(spyWrite).toHaveBeenCalledTimes(1)
    })
  })
})
