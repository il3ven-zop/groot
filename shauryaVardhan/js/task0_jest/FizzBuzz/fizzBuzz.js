function fizzBuzz(inputNumber) {
  if (isNaN(inputNumber)) {
    throw new Error('Input is invalid, it must be a number')
  }
  if (inputNumber < 1) {
    throw new Error('Input must be a number greater than 0')
  }
  const outputArray = Array(inputNumber).fill(0)

  outputArray.forEach((element, index) => {
    if ((index + 1) % 15 === 0) {
      outputArray[index] = 'FizzBuzz'
    } else if ((index + 1) % 5 === 0) {
      outputArray[index] = 'Buzz'
    } else if ((index + 1) % 3 === 0) {
      outputArray[index] = 'Fizz'
    } else {
      outputArray[index] = `${index + 1}`
    }
  })

  return outputArray
}

module.exports = fizzBuzz
