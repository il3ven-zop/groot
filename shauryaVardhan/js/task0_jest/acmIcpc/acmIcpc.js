function acmIcpc(topicArray) {
  if (!Array.isArray(topicArray)) {
    throw new Error('invalid input array')
  }
  const totalStudents = topicArray.length
  if (totalStudents < 2) {
    throw new Error('students should be greater than 1')
  }
  const totalSubjects = topicArray[0].length
  if (totalSubjects < 2) {
    throw new Error('subjects should be greater than 1')
  }
  let maxSubjectsKnown = 0
  let totalTeam = 0

  for (let iter = 0; iter < totalStudents - 1; iter++) {
    for (let iter2 = iter + 1; iter2 < totalStudents; iter2++) {
      let count = 0
      for (let iter3 = 0; iter3 < totalSubjects; iter3++) {
        if (
          topicArray[iter][iter3] === '1' ||
          topicArray[iter2][iter3] === '1'
        ) {
          count++
        }
      }
      if (count > maxSubjectsKnown) {
        maxSubjectsKnown = count
        totalTeam = 1
      } else if (count === maxSubjectsKnown) {
        totalTeam++
      }
    }
  }
  return [maxSubjectsKnown, totalTeam]
}

module.exports = acmIcpc
