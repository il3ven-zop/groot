const dogFetch = require('./dogFetch')
const axios = require('axios')
const path = require('path')
const fs = require('fs')

const initializeDogBreed = async (dogBreedName) => {
  await fs.writeFile(
    path.join(__dirname, 'dogBreed.txt'),
    dogBreedName,
    (err) => {
      if (err) console.log('failed')
    }
  )
}

beforeAll(() => {
  return initializeDogBreed('labrador')
})

describe('dogFetch', () => {
  test('should read the data from the file correctly', () => {
    return dogFetch
      .readFilePromise('dogBreed.txt')
      .then((data) => expect(data).toBe('labrador'))
  })

  test('should write the data to the file correctly', () => {
    return dogFetch
      .writeToFilePromise(
        {
          data: {
            message: 'someUrl'
          }
        },
        'dogBreed.txt'
      )
      .then((data) => expect(data).toBe('someUrl'))
  })

  test('should throw error if wrong filename is provided', async () => {
    await expect(dogFetch.readFilePromise('')).rejects.toStrictEqual(
      new Error('failed to read the file')
    )
  })

  test('should throw error if wrong filename is provided to fileToWriteTo', async () => {
    await expect(
      dogFetch.writeToFilePromise(
        {
          data: {
            message: 'someUrl'
          }
        },
        ''
      )
    ).rejects.toStrictEqual(new Error('failed to write to the file'))
  })

  test('should throw an error if breedName is incorrect', async () => {
    await initializeDogBreed('labradorean')
    await expect(dogFetch.dogFetch()).rejects.toStrictEqual(
      new Error('Request failed with status code 404')
    )
  })
  test('should make api request only once', async () => {
    initializeDogBreed('labrador')
    const spy = jest.spyOn(axios, 'get')
    await dogFetch.dogFetch()
    expect(spy).toHaveBeenCalledTimes(1)
  })
})
