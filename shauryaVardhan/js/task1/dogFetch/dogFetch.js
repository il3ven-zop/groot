const fs = require('fs')
const axios = require('axios')
const path = require('path')

const readFilePromise = (fileToReadFrom) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, fileToReadFrom), 'utf8', (err, data) => {
      if (err) reject(new Error('failed to read the file'))
      resolve(data)
    })
  })
}

const writeToFilePromise = (responseFromAxios, fileToWriteTo) => {
  const imageUrl = responseFromAxios?.data?.message
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, fileToWriteTo), imageUrl, (err) => {
      if (err) reject(new Error('failed to write to the file'))
      resolve(imageUrl)
    })
  })
}

const dogFetch = () => {
  return new Promise((resolve, reject) => {
    readFilePromise('dogBreed.txt')
      .then((dogBreedName) => {
        return axios.get(
          `https://dog.ceo/api/breed/${dogBreedName}/images/random`
        )
      })
      .then((imageUrl) => {
        return writeToFilePromise(imageUrl, 'dogImages.txt')
      })
      .then(() => {
        return resolve('successfully written the image in the file')
      })
      .catch((err) => {
        return reject(err)
      })
  })
}

module.exports = {
  readFilePromise,
  writeToFilePromise,
  dogFetch
}
