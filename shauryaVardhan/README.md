Name - **Shaurya Vardhan Singh**

College - **Jaypee Institute of Information Technology**

Hobbies - **Multiplayer Video Games, Partying**

# Js Tasks

**Task0** - Testing code using jest for the given three problems

- [Fizzbuzz](https://leetcode.com/problems/fizz-buzz/)
- [acmIcpcTeam](https://www.hackerrank.com/challenges/acm-icpc-team/problem)
- Binary search

**Task1** - Making an API request to get imageUrl of dog by reading dogBreed from a file and writing imageUrl to it using promises.
