const ACM = require('./ACM')

// standard test case
test('standard test case', () => {
  expect(ACM(['10101', '11110', '00010'])).toEqual([5, 1])
})

// if topic is not an Array
test('Should throw an Error', () => {
  expect(() => {
    ACM(25)
  }).toThrowError('Invalid Input')
})
