function knownSubjects(s1, s2) {
  let count = 0
  for (let i = 0; i < s1.length; i++) count += parseInt(s1[i]) | parseInt(s2[i])
  return count
}

function acmTeam(topics) {
  if (!Array.isArray(topics)) throw new Error('Invalid Input')

  const teams = []
  let maxSubjects = 0
  let teamsHavingMaxSubjects = 0

  for (let i = 0; i < topics.length; i++) {
    for (let j = i + 1; j < topics.length; j++) {
      const temp = knownSubjects(topics[i], topics[j])
      if (temp > maxSubjects) maxSubjects = temp
      teams.push(temp)
    }
  }

  teams.forEach((i) => {
    if (i === maxSubjects) teamsHavingMaxSubjects++
  })

  return [maxSubjects, teamsHavingMaxSubjects]
}

module.exports = acmTeam
