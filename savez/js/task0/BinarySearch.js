const binarySearch = (inputArray, key) => {
  if (!Array.isArray(inputArray) || typeof key !== 'number')
    throw new Error('Invalid Input')

  const sortedArray = [...inputArray].sort((a, b) => a - b)

  for (let i = 0; i < inputArray.length; i++)
    if (inputArray[i] !== sortedArray[i])
      throw new Error('Array must be sorted')

  let lowerBound = 0
  let upperBound = inputArray.length

  while (lowerBound <= upperBound) {
    const mid = Math.floor((lowerBound + upperBound) / 2)
    if (key === inputArray[mid]) return mid + 1
    if (key > inputArray[mid]) lowerBound = mid + 1
    else upperBound = mid - 1
  }

  return -1
}

module.exports = binarySearch
