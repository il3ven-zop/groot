const acm = require('./acm')

test('ans for test case 1', () => {
  expect(acm(['10101', '11110', '00010'])).toEqual([5, 1])
})
test('ans for test case 2', () => {
  expect(acm(['10101', '11100', '11010', '00101'])).toEqual([5, 2])
})
