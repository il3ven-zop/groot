const binarySearch = function (arr, n) {
  if (arr === null || !Array.isArray(arr) || typeof n !== 'number') {
    throw new Error('Invalid')
  }
  if (arr.length === 0) {
    return -1
  }

  let start = 0
  let end = arr.length - 1
  while (start <= end) {
    const mid = Math.floor((start + end) / 2)
    if (arr[mid] === n) {
      return true
    } else if (arr[mid] < n) {
      start = mid + 1
    } else {
      end = mid - 1
    }
  }
  return false
}
module.exports = binarySearch
