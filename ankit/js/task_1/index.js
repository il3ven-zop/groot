const fs = require('fs')
const axios = require('axios')
const path = require('path')

function readFile() {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path.join(__dirname, './breed.txt'),
      'utf8',
      function (err, data) {
        if (err) {
          return reject(new Error('file cant be read'))
        } else {
          resolve(data)
        }
      }
    )
  })
}

readFile()

function fetchData(name) {
  return axios.get(`https://dog.ceo/api/breed/${name}/images/random`)
}

function writeData(url) {
  return new Promise((resolve, reject) => {
    fs.writeFile('./result.txt', JSON.stringify(url), (err) => {
      if (err) {
        reject(new Error('file cant be write'))
      } else {
        resolve()
      }
    })
  })
}

function main() {
  return new Promise((resolve, reject) => {
    readFile().then((data) => {
      fetchData(data).then((response) => {
        writeData(response.data.message).then(() => {
          resolve(true)
        })
      })
    })
  })
}
module.exports = { readFile, writeData, fetchData, main }
