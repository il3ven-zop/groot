const { ESLint } = require('eslint')

;(async function main() {
  // 1. Create an instance.
  const eslint = new ESLint({ overrideConfigFile: './.eslintrc.json' })

  // 2. Lint files.
  const results = await eslint.lintFiles(['**/*.js'])
  // 4. Output it.
  const expression = /(?<=groot\/)[a-zA-Z]*/
  const consolidatedErrors = {}
  let count = 0
  results.forEach((result) => {
    const person = result.filePath.match(expression)[0]
    result.messages.forEach((message) => {
      count++
      if (!consolidatedErrors[person]) {
        consolidatedErrors[person] = []
        consolidatedErrors[person].push({
          error: message.message,
          filePath: result.filePath
        })
      } else {
        consolidatedErrors[person].push({
          error: message.message,
          filePath: result.filePath
        })
      }
    })
  })
  Object.keys(consolidatedErrors).forEach((person) => {
    consolidatedErrors[person].forEach((error, index) => {
      if (index === 0) {
        console.log('\x1b[37m', person.toUpperCase())
        console.log('------------------------------------------')
      }
      console.log('')
      console.error('\x1b[31m', 'ERROR:' + error.error)
      console.error('\x1b[33m', 'FILEPATH:' + error.filePath)
      console.log('')
    })
  })
  if (count > 0) {
    process.exitCode = 1
    console.error(`Found ${count} errors.`)
  }
})().catch((error) => {
  process.exitCode = 1
  console.error(error)
})
