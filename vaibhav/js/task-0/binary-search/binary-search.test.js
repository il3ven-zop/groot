const binarySearch = require('./binary-search')

describe('Test cases for Binary Search', () => {
  describe('Should return correct index for a given element in the array', () => {
    test('Find the element at index 0', () => {
      const inputArray = [1, 2, 3, 4]
      const inputElement = 1
      const output = 0

      expect(binarySearch(inputArray, inputElement)).toBe(output)
    })

    test('Find the element at index 1', () => {
      const inputArray = [1, 2, 3, 4]
      const inputElement = 2
      const output = 1

      expect(binarySearch(inputArray, inputElement)).toBe(output)
    })

    test('Find the element at index 2', () => {
      const inputArray = [1, 2, 3, 4]
      const inputElement = 3
      const output = 2

      expect(binarySearch(inputArray, inputElement)).toBe(output)
    })

    test('Find the element at index 3', () => {
      const inputArray = [1, 2, 3, 4]
      const inputElement = 4
      const output = 3

      expect(binarySearch(inputArray, inputElement)).toBe(output)
    })
  })

  test('Element cannot be found', () => {
    const inputArray = [1, 2, 3, 4]
    const inputElement = 5
    const output = -1

    expect(binarySearch(inputArray, inputElement)).toBe(output)
  })

  describe('Should throw error of Invalid Input', () => {
    test('When array is not sorted', () => {
      const inputArray = [4, 3, 2, 1]
      const inputElement = 4
      const output = 'Invalid Input'

      expect(() => {
        binarySearch(inputArray, inputElement)
      }).toThrow(output)
    })

    test('When array contains something other than numbers', () => {
      const inputArray = [1, 2, 3, '4']
      const inputElement = 4
      const output = 'Invalid Input'

      expect(() => {
        binarySearch(inputArray, inputElement)
      }).toThrow(output)
    })

    test('When array is empty', () => {
      const inputArray = []
      const inputElement = 4
      const output = 'Invalid Input'

      expect(() => {
        binarySearch(inputArray, inputElement)
      }).toThrow(output)
    })

    test('When input is not an Array', () => {
      const inputArray = {}
      const inputElement = 4
      const output = 'Invalid Input'

      expect(() => {
        binarySearch(inputArray, inputElement)
      }).toThrow(output)
    })
  })
})
