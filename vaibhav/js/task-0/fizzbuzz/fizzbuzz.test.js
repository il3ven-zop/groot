const fizzBuzz = require('./fizzbuzz')

describe('Test cases for FizzBuzz', () => {
  describe('Should return correct output when', () => {
    test('Input is 1', () => {
      const input = 1

      const output = ['1']

      expect(fizzBuzz(input)).toEqual(output)
    })

    test('Input is 3', () => {
      const input = 3

      const output = ['1', '2', 'Fizz']

      expect(fizzBuzz(input)).toEqual(output)
    })

    test('Input is 7', () => {
      const input = 7

      const output = ['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7']

      expect(fizzBuzz(input)).toEqual(output)
    })

    test('Input is 15', () => {
      const input = 15

      const output = [
        '1',
        '2',
        'Fizz',
        '4',
        'Buzz',
        'Fizz',
        '7',
        '8',
        'Fizz',
        'Buzz',
        '11',
        'Fizz',
        '13',
        '14',
        'FizzBuzz'
      ]

      expect(fizzBuzz(input)).toEqual(output)
    })
  })

  describe('Should throw an error when', () => {
    test('Input is negative', () => {
      const input = -1

      const output = 'Invalid Input'

      expect(() => {
        fizzBuzz(input)
      }).toThrow(output)
    })

    test('Input is zero', () => {
      const input = 0

      const output = 'Invalid Input'

      expect(() => {
        fizzBuzz(input)
      }).toThrow(output)
    })

    test('Input is a string', () => {
      const input = '15'
      const output = 'Invalid Input'

      expect(() => {
        fizzBuzz(input)
      }).toThrow(output)
    })

    test('Input is an object', () => {
      const input = {}
      const output = 'Invalid Input'

      expect(() => {
        fizzBuzz(input)
      }).toThrow(output)
    })
  })
})
