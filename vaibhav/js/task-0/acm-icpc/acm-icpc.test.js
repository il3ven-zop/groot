const acmTeam = require('./acm-icpc')

describe('Test cases for ACM ICPC', () => {
  describe('Correct input', () => {
    test("Should return [5, 2] for Input = ['10101', '11100', '11010', '00101']", () => {
      const input = ['10101', '11100', '11010', '00101']
      const output = [5, 2]

      expect(acmTeam(input)).toEqual(output)
    })

    test("Should return [5, 1] for Input = ['10101', '11110', '00010']", () => {
      const input = ['10101', '11110', '00010']
      const output = [5, 1]

      expect(acmTeam(input)).toEqual(output)
    })
  })

  describe('Should throw error', () => {
    test('Input is not an Array', () => {
      const input = 'Some string'
      const output = 'Invalid Input'

      expect(() => acmTeam(input)).toThrow(output)
    })

    test('Number of attendees is less than 2', () => {
      const input = [1]
      const output = 'Invalid Input'

      expect(() => acmTeam(input)).toThrow(output)
    })

    test('Number of attendees is greater than 500', () => {
      const input = new Array(501)
      const output = 'Invalid Input'

      expect(() => acmTeam(input)).toThrow(output)
    })

    test('Number of topics is less than 1', () => {
      const input = ['', '']
      const output = 'Invalid Input'

      expect(() => acmTeam(input)).toThrow(output)
    })

    test('Number of topics is greater than 500', () => {
      const input = ['1'.repeat(501), '0'.repeat(501)]
      const output = 'Invalid Input'

      expect(() => acmTeam(input)).toThrow(output)
    })
  })
})
