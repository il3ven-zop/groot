function countSubjects(team1, team2) {
  let result = 0

  for (let i = 0; i < team1.length; i++) {
    if (team1[i] === '1' || team2[i] === '1') {
      result++
    }
  }

  return result
}

/**
 * HackerRank Question: https://www.hackerrank.com/challenges/acm-icpc-team/problem
 * @param {string[]} topic
 * @returns {number[]}
 */
function acmTeam(topic) {
  if (!Array.isArray(topic)) throw new Error('Invalid Input')
  if (topic.length < 2 || topic.length > 500) throw new Error('Invalid Input')
  if (topic[0].length < 1 || topic[0].length > 500)
    throw new Error('Invalid Input')

  const result = []

  for (let i = 0; i < topic.length; i++) {
    for (let j = i + 1; j < topic.length; j++) {
      result.push(countSubjects(topic[i], topic[j]))
    }
  }

  const max = Math.max(...result)

  return [max, result.filter((elm) => elm === max).length]
}

module.exports = acmTeam
