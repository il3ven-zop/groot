const fs = require('fs/promises')
const getAndSaveImage = require('./task-2')
const axios = require('axios')

jest.mock('axios')
jest.mock('fs/promises')

describe('Test cases for Fetching and Saving Dog Images (Using async/await)', () => {
  const SUCCESS = 'Success'
  const BREED_NAME = 'hound'
  const BREED_FILE_NAME = 'breed.txt'
  const SUCCESS_RESP = {
    data: {
      message: [
        'https://images.dog.ceo/breeds/hound-afghan/n02088094_3119.jpg'
      ],
      status: 'success'
    }
  }

  beforeEach(() => {
    axios.get.mockResolvedValue(SUCCESS_RESP)

    fs.readFile.mockImplementation(async (_) => {
      return Buffer.from(BREED_NAME)
    })

    fs.writeFile.mockImplementation(async (_, __) => {
      return undefined
    })
  })

  test('Should return Success', async () => {
    const ret = await getAndSaveImage(BREED_FILE_NAME)
    expect(ret).toBe(SUCCESS)
  })

  describe('Should throw error', () => {
    test('When API is down', async () => {
      axios.get.mockImplementationOnce(() =>
        Promise.reject(new Error('Cannot fetch from API'))
      )

      expect.assertions(1)
      return getAndSaveImage(BREED_FILE_NAME).catch((err) =>
        expect(err.toString()).toBe('Error: Cannot fetch data')
      )
    })

    test('When filename is invalid', () => {
      const INVALID_FILE_NAME = 'random.txt'

      fs.readFile.mockImplementationOnce(async (_) => {
        throw new Error()
      })

      expect.assertions(1)
      return getAndSaveImage(INVALID_FILE_NAME).catch((err) => {
        expect(err.toString()).toEqual('Error: Cannot read file')
      })
    })

    test('When writeFile fails', () => {
      fs.writeFile.mockImplementationOnce((_, __, cb) => {
        throw new Error()
      })

      expect.assertions(1)
      return getAndSaveImage(BREED_FILE_NAME).catch((err) => {
        expect(err.toString()).toEqual('Error: Cannot write file')
      })
    })

    test('When text file is empty', () => {
      fs.readFile.mockImplementationOnce(async (_) => {
        return ''
      })

      expect.assertions(1)
      return getAndSaveImage(BREED_FILE_NAME).catch((err) => {
        expect(err.toString()).toEqual('Error: Cannot read file')
      })
    })
  })
})
