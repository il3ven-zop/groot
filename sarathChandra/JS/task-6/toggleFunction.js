// TODO: remove ignore
/* istanbul ignore file */
// eslint-disable-next-line no-unused-vars
function toggleFunction() {
  const checkBox = document.getElementById('checkBox')
  const basicPrice = document.getElementById('basicPrice')
  const proPrice = document.getElementById('proPrice')
  const masterPrice = document.getElementById('masterPrice')
  basicPrice.innerHTML = checkBox.checked === true ? '$19.99' : '$199.99'
  proPrice.innerHTML = checkBox.checked === true ? '$24.99' : '$249.99'
  masterPrice.innerHTML = checkBox.checked === true ? '$39.99' : '$399.99'
}
