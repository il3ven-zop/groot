const roboHash = require('./roboHash')
const fs = require('fs')

const fileExists = (file) => {
  try {
    if (fs.existsSync('./Task-2/imagesGenerated/' + file)) {
      return true
    } else {
      return false
    }
  } catch (err) {
    console.error(err)
  }
}

describe('Test roboHash()', () => {
  test('should check if files exist', async () => {
    const stringLengths = [1, 2, 3, 4, 5]
    await roboHash(stringLengths)
    const responses = [
      fileExists('1.png'),
      fileExists('2.png'),
      fileExists('3.png'),
      fileExists('4.png'),
      fileExists('5.png')
    ]
    expect(responses).toBeTruthy()
  })
  test('should check if file exists with undefined length', async () => {
    const stringLengths = [1, 2, 3, 4, 5, undefined]
    await roboHash(stringLengths)
    const response = fileExists('6.png')
    expect(response).toBeFalsy()
  })
})
