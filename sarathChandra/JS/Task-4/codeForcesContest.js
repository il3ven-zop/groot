const fetch = require('node-fetch')

function getUsers(contestId) {
  return fetch(
    `https://codeforces.com/api/contest.standings?contestId=${contestId}&from=1&count=10&showUnofficial=true`
  ).then((res) => res.json())
}

function getRatings(userHandle) {
  return fetch(
    `https://codeforces.com/api/user.rating?handle=${userHandle}`
  ).then((res) => res.json())
}

function filterRows(contest) {
  const userMembers = contest.result.rows.filter((user) => {
    return user.party.members.length === 1
  })
  return userMembers
}

function extractMemberHandles(userMembers) {
  const memberHandles = []
  userMembers.forEach((item) => {
    memberHandles.push(item.party.members[0].handle)
  })
  return memberHandles
}

function getNewRatings(res) {
  const newRatings = []
  const newObj = {}
  res.forEach((item) => {
    let handle = ''
    item.result.forEach((innerItem) => {
      newRatings.push(innerItem.newRating)
      handle = innerItem.handle
    })
    newObj[handle] = newRatings
  })
  return newObj
}

async function contestInfo(contestId) {
  try {
    const contest = await getUsers(contestId.toString())
    const userMembers = filterRows(contest)
    console.log('filtered rows with 1 member:')
    console.log(JSON.stringify(userMembers))
    const memberHandles = extractMemberHandles(userMembers)
    console.log('\n extracted member handles:')
    console.log(memberHandles)
    const res = await Promise.all(memberHandles.map(getRatings))
    const newObj = getNewRatings(res)
    console.log('\n extracted all newRatings of handles:')
    console.log(JSON.stringify(newObj))
  } catch (error) {
    console.error(error.message)
  }
}

module.exports = getUsers
module.exports = filterRows
module.exports = contestInfo
