const fs = require('fs')
const fetch = require('fetch')

// utility functions:
const generateRandomString = (stringLength) => {
  let result = ''
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < stringLength; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

// promises:
const stringGenerationPromise = (stringLength) => {
  return new Promise((resolve, reject) => {
    if (typeof stringLength !== 'number') reject(new Error(''))
    resolve(generateRandomString(stringLength))
  })
}

const imageRequestPromise = (string) => {
  return new Promise((resolve, reject) => {
    fetch.fetchUrl(`https://robohash.org/${string}`, (e, meta, data) => {
      resolve(data)
    })
  })
}

const writeFilePromise = (data, fileName) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, data, () => {
      resolve()
    })
  })
}

async function roboHashAsyncAwait(stringLength, roboName) {
  try {
    const string = await stringGenerationPromise(stringLength)
    const data = await imageRequestPromise(string)
    await writeFilePromise(data, roboName)
  } catch (err) {}
}

module.exports = roboHashAsyncAwait
