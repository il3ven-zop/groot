const userInfo = require('./codeForcesUserInfo')

describe('Test codeForcesUserInfo()', () => {
  test('should return Udit Sanghi', async () => {
    const userHandles = [
      'lokpati',
      'Ashishgup',
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await userInfo(userHandles)
    expect(response).toBe('Udit Sanghi')
  })
})
