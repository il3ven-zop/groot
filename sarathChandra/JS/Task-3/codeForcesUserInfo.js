const fetch = require('node-fetch')

function getUsers(userHandles) {
  let userHandleString = ''
  userHandles.forEach((user) => {
    userHandleString = userHandleString + user + ';'
  })
  return fetch(
    `https://codeforces.com/api/user.info?handles=${userHandleString}`
  ).then((res) => res.json())
}

async function userInfo(userHandles) {
  const users = await getUsers(userHandles)
  let topperRating = -Infinity
  users.result.forEach((user) => {
    topperRating = Math.max(topperRating, user.maxRating)
  })
  const topper = users.result.filter((element) => {
    return element.maxRating === topperRating
  })
  return topper[0].firstName + ' ' + topper[0].lastName
}

module.exports = userInfo
