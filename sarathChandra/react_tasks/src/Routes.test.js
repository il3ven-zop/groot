import React from 'react'
import { configure } from 'enzyme'
import { Routes } from './Routes'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
import { BrowserRouter } from 'react-router-dom'
configure({ adapter: new Adapter() })

describe('Routes', () => {
  it('should render Routes correctly', () => {
    const tree = renderer
      .create(
        <BrowserRouter>
          {' '}
          <Routes />
        </BrowserRouter>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
