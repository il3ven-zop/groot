import React from 'react'
import { configure } from 'enzyme'
import { NavBar } from './NavBar'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
import { BrowserRouter } from 'react-router-dom'
configure({ adapter: new Adapter() })

describe('NavBar', () => {
  it('should render NavBar correctly', () => {
    const tree = renderer
      .create(
        <BrowserRouter>
          <NavBar />
        </BrowserRouter>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
