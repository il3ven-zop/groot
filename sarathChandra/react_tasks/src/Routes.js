import React from 'react'
import { Route, Switch } from 'react-router-dom'
import TaskOne from './task1/task1'

export const Routes = () => {
  return (
    <div>
      <Switch>
        <Route path="/task1" exact component={TaskOne} />
      </Switch>
    </div>
  )
}
