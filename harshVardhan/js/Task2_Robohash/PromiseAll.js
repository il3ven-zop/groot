const saveImage = require('./AsyncAwait')

function saveAllImages() {
  const images = ['1', '2', '3', '4', '5']

  return Promise.all(
    images.map((element) => saveImage(element))
  ).then((value) => value.every((element) => element))
}

saveAllImages()

module.exports = saveAllImages
