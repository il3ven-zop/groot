const saveImage = require('./AsyncAwait.js')

describe('Robohash', () => {
  test('Should fetch and save image in a file', async () => {
    // Testing for a correct url
    const expectedResult = await saveImage(0)
    expect(expectedResult).toEqual(true)
  })
})
