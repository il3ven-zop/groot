const axios = require('axios').default
const fs = require('fs')

// Promise for Generating Random String
const generateString = function () {
  return new Promise((resolve, reject) => {
    const randomString = Math.random().toString(36).substring(2, 10) // Generating Random String

    resolve(randomString)
    reject(Error('String Not Generated'))
  })
}

// Promise for Appending Random String to URL
const appendString = function (randomString) {
  return new Promise((resolve, reject) => {
    const baseurl = 'https://robohash.org/'
    const newurl = baseurl + randomString // Concatinating Random String to url

    resolve(newurl)
    reject(Error('String Not Appended'))
  })
}

// Promise for Fetching data from url
const fetchResult = function (url) {
  return new Promise((resolve, reject) => {
    const fetchdata = axios.get(url, { responseType: 'arraybuffer' })

    resolve(fetchdata)
    reject(Error('Data not fetched'))
  })
}

// Promise for writing Image in the file
const writeFile = function (fetchdata, index) {
  return new Promise((resolve, reject) => {
    fs.writeFile(`${index}Image.png`, fetchdata.data, function () {
      // Creating File to Store an Image
      resolve(true)
    })
  })
}

async function saveImage(index) {
  const randomstring = await generateString()
  const newurl = await appendString(randomstring)
  const filedata = await fetchResult(newurl)
  const finalresult = await writeFile(filedata, index)
  return finalresult
}

saveImage(0)

module.exports = saveImage
