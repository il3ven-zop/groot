const fizzbuzz = require('./fizzbuzz')

describe('fizzbuzz', () => {
  test('should throw error when input is not a Number', () => {
    const result = () => fizzbuzz('abcd')
    expect(result).toThrow('Invalid input')
  })

  test('should throw error when input is less than 0', () => {
    const result = () => fizzbuzz(-20)
    expect(result).toThrow('Invalid input')
  })

  test('should return fizzbuzz when input is multiple of 3 and 5', () => {
    const result = fizzbuzz(15)
    const expectedResult = 'fizzbuzz'
    expect(result).toEqual(expectedResult)
  })
  test('should return buzz when input is multiple of 5', () => {
    const result = fizzbuzz(5)
    const expectedResult = 'buzz'
    expect(result).toEqual(expectedResult)
  })

  test('should return fizz when input is multiple of 3', () => {
    const result = fizzbuzz(3)
    const expectedResult = 'fizz'
    expect(result).toEqual(expectedResult)
  })

  test('should return the input value when input is not multiple of 3 and 5', () => {
    const result = fizzbuzz(2)
    const expectedResult = '2'
    expect(result).toEqual(expectedResult)
  })
})
