function fizzbuzz(n) {
  if (isNaN(n) || n < 1) {
    throw new Error('Invalid input')
  }

  if (n % 15 === 0) {
    return 'fizzbuzz'
  } else if (n % 5 === 0) {
    return 'buzz'
  } else if (n % 3 === 0) {
    return 'fizz'
  } else {
    return n.toString()
  }
}

module.exports = fizzbuzz
