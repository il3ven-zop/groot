const binarySearch = require('./binarySearch')

describe('binarySearch', () => {
  test('should return error when array is not array', () => {
    const bs = () => binarySearch(0, 4, 1, undefined)
    expect(bs).toThrow('Invalid input')
  })

  test('should return error when element to be search is not a number', () => {
    const bs = () => binarySearch(0, 6, 't', [1, 2, 3, 4, 5, 6, 7])
    expect(bs).toThrow('Invalid input')
  })

  test('should return -1 when number to be search is not found', () => {
    const bs = binarySearch(0, 6, 8, [1, 2, 3, 4, 5, 6, 7])
    expect(bs).toBe(-1)
  })

  test('should return index 3 when searching middle', () => {
    const bs = binarySearch(0, 6, 4, [1, 2, 3, 4, 5, 6, 7])
    expect(bs).toBe(3)
  })

  test('should return index 0 when searching leftmost', () => {
    const bs = binarySearch(0, 6, 1, [1, 2, 3, 4, 5, 6, 7])
    expect(bs).toBe(0)
  })

  test('should return index 6, when searching rightmost', () => {
    const bs = binarySearch(0, 6, 7, [1, 2, 3, 4, 5, 6, 7])
    expect(bs).toBe(6)
  })
})
