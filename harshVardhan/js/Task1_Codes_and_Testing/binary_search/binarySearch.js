function binarySearch(left, right, number, array) {
  if (!Array.isArray(array) || isNaN(number)) {
    throw new Error('Invalid input')
  }

  while (left <= right) {
    const mid = left + Math.floor((right - left) / 2)
    if (array[mid] === number) return mid
    else if (array[mid] > number) right = mid - 1
    else left = mid + 1
  }
  return -1
}

module.exports = binarySearch
