import Login from './task1/components/forms/login.jsx'
import Register from './task1/components/forms/register.jsx'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import React from 'react'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/login" component="Login">
          <Login />
        </Route>
        <Route path="/register" component="Register ">
          <Register />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default App
