import React, { useState } from 'react'
import Username from '../username'
import Email from '../email'
import Password from '../password/Password.jsx'
import { AlreadyUser } from '../button/AlreadyUserBtn'
import Header from '../Header'
import { RegisterButton } from '../button/RegisterBtn'
import './forms.css'

const Register = () => {
  const [emailStatus, setEmailStatus] = useState({ emailStatus: true })
  const [passwordStatus, setPasswordStatus] = useState({
    passwordStatus: true
  })
  const [usernameStatus, setUsernameStatus] = useState({
    usernameStatus: true
  })

  return (
    <div>
      <Header />
      <section className="register">
        <form name="register" className="form">
          <h1>Sign Up</h1>

          <Username usernameValidate={setUsernameStatus} />
          <Email emailValidate={setEmailStatus} />
          <Password passwordValidate={setPasswordStatus} />
          <RegisterButton
            value={
              /* istanbul ignore next */ emailStatus ||
              passwordStatus ||
              usernameStatus
            }
          />
          <AlreadyUser />
        </form>
      </section>
    </div>
  )
}

export default Register
