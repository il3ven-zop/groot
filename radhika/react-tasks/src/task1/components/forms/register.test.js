import React from 'react'
import renderer from 'react-test-renderer'
import Register from './register'

describe('register', () => {
  test('register must render', () => {
    const tree = renderer.create(<Register />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
