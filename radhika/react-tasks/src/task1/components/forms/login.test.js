import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { render, fireEvent } from '@testing-library/react'
import Login from './login'

describe('login', () => {
  test('function must be calld on click', () => {
    const mockOnClick = jest.fn()
    const BtnComponent = render(
      <BrowserRouter>
        <Login />
      </BrowserRouter>
    )
    const btn = BtnComponent.getByTestId('login')
    btn.addEventListener('click', mockOnClick)
    fireEvent.click(btn)
    setTimeout(() => {
      expect(mockOnClick).toHaveBeenCalledTimes(1)
    }, 1000)
  })
})
