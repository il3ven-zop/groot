import React, { useState } from 'react'

export const EMAIL_REQUIRED_MESSAGE = 'Email required'
export const EMAIL_INVALID_MESSAGE = 'Invalid Email'

// eslint-disable-next-line react/prop-types
const Email = ({ emailValidate }) => {
  const [email, setEmail] = useState('')
  const [error, setError] = useState('')

  const emailValidation = (event) => {
    const email = event?.target?.value
    let valid = false
    setEmail(email)
    const regex = /([a-z\d.-]+)@([a-z\d-]+)\.([a-z]{2,8})/
    if (!email) {
      setError(EMAIL_REQUIRED_MESSAGE)
      valid = true
    } else if (email.match(regex)) setError('')
    else {
      setError(EMAIL_INVALID_MESSAGE)
      valid = true
    }
    emailValidate(valid)
  }

  return (
    <div data-testid="container">
      <input
        className="form_content-input"
        type="email"
        name="email"
        placeholder="email"
        onChange={emailValidation}
        value={email}
      />
      <span data-testid="error">{error}</span>
    </div>
  )
}

export default Email
