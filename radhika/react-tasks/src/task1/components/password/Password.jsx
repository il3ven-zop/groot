import React, { useState } from 'react'

export const PASSWORD_INVALID_MESSAGE = 'Length of password must be atleast 8'
export const PASSWORD_REQUIRED_MESSAGE = 'Password required'
export const PASSWORD_NOT_MATCHED_MESSAGE = 'Password not matched'

// eslint-disable-next-line react/prop-types
const Password = ({ passwordValidate }) => {
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [passwordError, setPasswordError] = useState('')
  const [confirmPasswordError, setConfirmPasswordError] = useState('')

  const passwordValidation = (event) => {
    const password = event?.target?.value
    let valid = false
    setPassword(password)
    if (!password) {
      setPasswordError(PASSWORD_REQUIRED_MESSAGE)
      valid = true
    } else if (password.length < 8) {
      setPasswordError(PASSWORD_INVALID_MESSAGE)
      valid = true
    } else setPasswordError('')

    passwordValidate(valid)
  }
  const confirmPasswordValidation = (event) => {
    const confirmPassword = event?.target?.value
    let valid = false
    setConfirmPassword(confirmPassword)
    if (password === confirmPassword) {
      setConfirmPasswordError('')
    } else if (!confirmPassword) {
      setConfirmPasswordError(PASSWORD_REQUIRED_MESSAGE)
      valid = true
    } else {
      setConfirmPasswordError(PASSWORD_NOT_MATCHED_MESSAGE)
      valid = true
    }
    passwordValidate(valid)
  }
  return (
    <div>
      <input
        className="form_content-input"
        type="password"
        placeholder="password"
        onChange={passwordValidation}
        value={password}
      />
      <span data-testid="passwordError">{passwordError}</span>
      <input
        className="form_content-input"
        type="password"
        placeholder="confirm password"
        onChange={confirmPasswordValidation}
        value={confirmPassword}
      ></input>
      <span data-testid="confirmPasswordError">{confirmPasswordError}</span>
    </div>
  )
}
export default Password
