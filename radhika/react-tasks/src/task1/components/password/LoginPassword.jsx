import React, { useState } from 'react'

export const PASSWORD_REQUIRED_MESSAGE = 'Password required'

// eslint-disable-next-line react/prop-types
const Password = ({ passwordValidate }) => {
  const [password, setPassword] = useState('')
  const [passwordError, setPasswordError] = useState('')

  const passwordValidation = (event) => {
    const password = event?.target?.value
    let valid = false
    setPassword(password)

    if (!password) {
      setPasswordError('Password required')
      valid = true
    } else setPasswordError('')

    passwordValidate(valid)
  }

  return (
    <div>
      <input
        className="form_content-input"
        type="password"
        placeholder="password"
        onChange={passwordValidation}
        value={password}
      />
      <span data-testid="passwordError">{passwordError}</span>
    </div>
  )
}
export default Password
