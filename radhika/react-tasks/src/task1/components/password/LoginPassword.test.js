import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import LoginPassword, { PASSWORD_REQUIRED_MESSAGE } from './LoginPassword.jsx'

const setup = () => {
  const mockFunc = jest.fn()
  const PasswordComponent = render(
    <LoginPassword passwordValidate={mockFunc} />
  )
  const inputPassword = PasswordComponent.getByPlaceholderText('password')
  return {
    inputPassword,
    ...PasswordComponent
  }
}

describe('PASSWORD Validation', () => {
  test('Is PASSWORD Valid', () => {
    const { inputPassword, ...PasswordComponent } = setup()
    fireEvent.change(inputPassword, { target: { value: '12345678' } })
    const errorValue = PasswordComponent.getByTestId('passwordError')
      .textContent
    expect(errorValue).toBe('')
  })
  test('password is required', () => {
    const { inputPassword, ...PasswordComponent } = setup()
    fireEvent.change(inputPassword, { target: { value: '1234567' } })
    fireEvent.change(inputPassword, { target: { value: null } })
    const errorValue = PasswordComponent.getByTestId('passwordError')
      .textContent
    expect(errorValue).toBe(PASSWORD_REQUIRED_MESSAGE)
  })
})
