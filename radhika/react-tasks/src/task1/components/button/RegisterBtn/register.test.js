import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import RegisterBtn from './index.jsx'

describe('Register Button', () => {
  test('Register button must render', () => {
    const mockOnClick = jest.fn()
    const BtnComponent = render(<RegisterBtn />)
    const btn = BtnComponent.getByTestId('register')
    btn.addEventListener('click', mockOnClick)
    fireEvent.click(btn)
    setTimeout(() => {
      expect(mockOnClick).toHaveBeenCalledTimes(1)
    }, 1000)
  })
})
