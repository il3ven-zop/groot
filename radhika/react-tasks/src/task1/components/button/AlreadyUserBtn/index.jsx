import React from 'react'
import { BrowserRouter as Router, Route, useHistory } from 'react-router-dom'
import Register from '../../forms/register'

export const AlreadyUser = () => {
  const history = useHistory()
  const handleClick = () => history.push('/login')
  return (
    <button
      className="form_content-btn"
      onClick={handleClick}
      data-testid="AlreadyUser"
    >
      Already a user? Login
    </button>
  )
}

function AlreadyUserApp() {
  return (
    <Router>
      <Route path="/register" exact component={Register} />
      <Route path="/" exact component={AlreadyUser} />
    </Router>
  )
}

export default AlreadyUserApp
