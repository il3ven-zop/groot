import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import AlreadyUser from './index'

describe('Already a user onclick', () => {
  test('onclick must redirect to login page', () => {
    const mockOnClick = jest.fn()
    const BtnComponent = render(<AlreadyUser />)
    const btn = BtnComponent.getByTestId('AlreadyUser')
    btn.addEventListener('click', mockOnClick)
    fireEvent.click(btn)
    setTimeout(() => {
      expect(mockOnClick).toHaveBeenCalledTimes(1)
    }, 1000)
  })
})
