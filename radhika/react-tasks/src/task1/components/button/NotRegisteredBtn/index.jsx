import React from 'react'
import { BrowserRouter as Router, Route, useHistory } from 'react-router-dom'
import Register from '../../forms/register'

export const NotRegistered = () => {
  const history = useHistory()
  const handleClick = () => history.push('/register')
  return (
    <button
      className="form_content-btn"
      onClick={handleClick}
      data-testid="NotRegistered"
    >
      New User? Register
    </button>
  )
}
function NotRegisterApp() {
  return (
    <Router>
      <Route path="/register" exact component={Register} />
      <Route path="/" exact component={NotRegistered} />
    </Router>
  )
}

export default NotRegisterApp
