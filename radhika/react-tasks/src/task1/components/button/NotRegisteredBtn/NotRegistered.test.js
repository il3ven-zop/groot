import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import NotRegistered from './index'

describe('Already a user onclick', () => {
  test('onclick must redirect to login page', () => {
    const mockOnClick = jest.fn()
    const BtnComponent = render(<NotRegistered />)
    const btn = BtnComponent.getByTestId('NotRegistered')
    btn.addEventListener('click', mockOnClick)
    fireEvent.click(btn)
    setTimeout(() => {
      expect(mockOnClick).toHaveBeenCalledTimes(1)
    }, 1000)
  })
})
