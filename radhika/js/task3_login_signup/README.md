**Task 3 :** _Login and SignUp Page_

### Description

This task is done using html,css and js.\
login.html is the layout for login page.\
register.html is the lyout for register page.\
css/style.css is use for styling.\
js/script.js is used for validations.

### Demo

![ALT TEXT](./Demo/login_signup.gif)
