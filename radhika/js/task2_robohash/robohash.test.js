const fs = require('fs')
const axios = require('axios')
const {
  generateImage,
  generateUrl,
  getAndSaveImage,
  getImage
} = require('./robohash')

describe('robohash', () => {
  afterEach(() => jest.restoreAllMocks())
  describe('generateUrl', () => {
    test('should generate random string', async () => {
      jest.spyOn(Math, 'random').mockImplementation(() => '1234567')
      const expectedOutput = 'https://robohash.org/1234567'
      const res = await generateUrl()
      expect(res).toStrictEqual(expectedOutput)
    })
  })

  describe('fsWriteFileSpy', () => {
    test('should generate file with image data', async () => {
      const spy = jest
        .spyOn(fs, 'writeFile')
        .mockImplementation((_, __, cb) => {
          cb()
        })

      await getAndSaveImage('dummy input', 0, 'index')
      expect(spy).toHaveBeenCalledTimes(1)
    })

    test('should reject if file not generated', async () => {
      jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
        cb(new Error('something went wrong'))
      })

      await expect(
        getAndSaveImage('dummy input', 0, 'index')
      ).rejects.toMatchSnapshot()
    })
  })

  describe('axiosGetSpy', () => {
    test('should fetch url', async () => {
      const spy = jest.spyOn(axios, 'get').mockImplementation((url) => {
        return {}
      })
      await getImage('www.example.com')
      expect(spy).toHaveBeenCalledTimes(1)
    })
  })
  describe('generateImage', () => {
    test('promise is resolved and image generated successfully', async () => {
      const status = await generateImage(0)
      expect(status).toBe(true)
    })
  })
})
