const axios = require('axios')
const fs = require('fs')

function generateUrl() {
  return new Promise((resolve) => {
    const randIndex = Math.random()
    const randomString = randIndex.toString(36).substring(0, 8)
    const url = 'https://robohash.org/' + randomString
    resolve(url)
  })
}

function getImage(url) {
  return new Promise((resolve) => {
    const config = {
      responseType: 'arraybuffer'
    }
    const res = axios.get(url, config)
    resolve(res)
  })
}

function getAndSaveImage(res, index, location) {
  return new Promise((resolve, reject) => {
    fs.writeFile(location + 'image' + index + '.png', res.data, (err) => {
      if (err) {
        reject(new Error(err.message))
      }
      resolve(true)
    })
  })
}

const generateImage = async (index) => {
  const getApiUrl = await generateUrl()
  const imageData = await getImage(getApiUrl)
  return await getAndSaveImage(imageData, index, './')
}

module.exports = {
  generateImage,
  generateUrl,
  getAndSaveImage,
  getImage
}
