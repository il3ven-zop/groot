const acmTeam = require('./acm_ipc_team.js')

describe('acmTeam', () => {
  test('check if input is empty', () => {
    const topic = []
    const output = () => {
      acmTeam(topic)
    }
    expect(output).toThrow('Invalid input')
  })
  test('sum of subjects is zero', () => {
    const topic = [
      [0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0]
    ]

    const output = acmTeam(topic)
    const expectedOutput = [0, 3]
    expect(output).toStrictEqual(expectedOutput)
  })
  test('check if total subject known is adding correctly to the count of list', () => {
    const topic = [
      [1, 0, 0, 0, 0, 1],
      [1, 0, 1, 0, 0, 0],
      [1, 0, 1, 1, 1, 1]
    ]
    const output = acmTeam(topic)

    const expectedOutput = [5, 2]
    expect(output).toStrictEqual(expectedOutput)
  })
  test('check if max is not exceeded in the count of test', () => {
    const topic = [
      [1, 0, 0, 0, 0, 1],
      [1, 1, 1, 0, 0, 0],
      [1, 0, 1, 1, 1, 1]
    ]
    const output = acmTeam(topic)

    const expectedOutput = [6, 1]
    expect(output).toStrictEqual(expectedOutput)
  })
  test('max no of subjects = total count then no of teams must be greater than 0', () => {
    const topic = [
      [0, 0, 0, 0, 0, 0],
      [1, 1, 1, 1, 1, 1],
      [0, 0, 0, 0, 0, 0],
      [1, 1, 1, 1, 1, 1]
    ]
    const output = acmTeam(topic)

    const expectedOutput = [6, 5]
    expect(output).toEqual(expectedOutput)
  })
})
