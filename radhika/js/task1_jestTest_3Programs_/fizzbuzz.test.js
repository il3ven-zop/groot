const fizzbuzz = require('./fizzbuzz')

describe('fizzbuzz', () => {
  test('Input is not number or input is less than 0 or input i greater than 100 throw error', () => {
    const output = () => {
      fizzbuzz(-1)
    }
    expect(output).toThrow('Invalid Input')
  })
  test('divisible by 15 return fizzbuzz', () => {
    const output = fizzbuzz(15)

    expect(output).toEqual('fizzbuzz')
  })
  test('dicisible by 5 return buzz', () => {
    const output = fizzbuzz(5)

    expect(output).toEqual('buzz')
  })

  test('divisible by 3 return fizz', () => {
    const output = fizzbuzz(3)

    expect(output).toEqual('fizz')
  })

  test('not divisible by 5,3,or 15 then print the number', () => {
    const output = fizzbuzz(4)

    expect(output).toEqual(4)
  })
})
