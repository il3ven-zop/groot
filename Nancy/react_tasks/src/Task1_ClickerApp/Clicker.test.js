import React from 'react'
import { render, fireEvent, getByTestId } from '@testing-library/react'
import renderer from 'react-test-renderer'
import Clicker from './Clicker'

let container = null
let value = null

beforeEach(() => {
  container = render(<Clicker />).container
  value = getByTestId(container, 'value-display')
})

afterEach(() => {
  container = null
})

it('renders correctly', () => {
  const tree = renderer.create(<Clicker />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('clicker', () => {
  expect(value.textContent).toBe('0')
})

it('increment button works', () => {
  const incrementValue = getByTestId(container, 'increment')
  fireEvent.click(incrementValue)
  expect(value.textContent).toBe('1')
})

it('refresh the value', () => {
  const incrementValue = getByTestId(container, 'increment')
  fireEvent.click(incrementValue)
  fireEvent.click(incrementValue)
  expect(value.textContent).toBe('2')
  const refreshValue = getByTestId(container, 'refresh')
  fireEvent.click(refreshValue)
  expect(value.textContent).toBe('0')
})

it('decrement button works', () => {
  const decrementValue = getByTestId(container, 'decrement')
  fireEvent.click(decrementValue)
  expect(value.textContent).toBe('-1')
})
