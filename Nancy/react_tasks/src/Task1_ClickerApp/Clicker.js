import React, { useState } from 'react'
import './Clicker.css'

export default function Clicker() {
  const [clickerValue, updateClickerValue] = useState(0)
  return (
    <div className="App">
      <div className="container">
        <div className="clicker-parent">
          <div data-testid="value-display" className="clicker-display">
            {clickerValue}
          </div>
          <div className="clicker-button-container">
            <button
              data-testid="increment"
              className="btn-plus"
              onClick={() => updateClickerValue(clickerValue + 1)}
            >
              <i className="fa fa-5x fa-plus" />
            </button>
            <button
              data-testid="refresh"
              className="btn-refresh"
              onClick={() => updateClickerValue(0)}
            >
              <i className="fa fa-5x fa-refresh" />
            </button>
            <button
              data-testid="decrement"
              className="btn-minus"
              onClick={() => updateClickerValue(clickerValue - 1)}
            >
              <i className="fa fa-5x fa-minus" />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
