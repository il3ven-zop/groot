import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Clicker from './Task1_ClickerApp/Clicker'
import Navbar from './Navbar'

const App = () => {
  return (
    <React.Fragment>
      <Navbar />
      <main>
        <Switch>
          <Route path="/home" />
          <Route path="/Task1" component={Clicker} />
        </Switch>
      </main>
    </React.Fragment>
  )
}

export default App
