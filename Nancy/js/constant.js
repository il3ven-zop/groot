const ROBO_URL = 'https://robohash.org/'
const CODEFORCES_USER_URL = 'http://codeforces.com/api/user.info'
const ROBO_NAME = 'robo'

module.exports = {
  ROBO_URL,
  CODEFORCES_USER_URL,
  ROBO_NAME
}
