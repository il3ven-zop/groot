const axios = require('axios')
const path = require('path')
const fs = require('fs')
const {
  readDogBreed,
  writeDogBreedUrl,
  fetchDogApiUrl,
  fetchStoreImageUrl
} = require('./fetchDogBreedURL')

beforeEach(() => jest.restoreAllMocks())
afterAll(() => {
  fs.unlinkSync(path.join(__dirname, './dogBreedImageURL.txt'))
})

describe('FetchDogApiUrl:', () => {
  test('fetchDogApiUrl should return a message(something.jpeg)', () => {
    jest.spyOn(axios, 'get').mockImplementation(() => {
      return Promise.resolve({
        data: {
          message: 'something.jpeg',
          status: 'success'
        }
      })
    })

    return fetchDogApiUrl('arbitrary.co').then((message) => {
      expect(message).toEqual('something.jpeg')
    })
  })

  test('fetchDogApiUrl should throw a error on fetch fail', () => {
    const fetchFailError = new Error('Could not fetch the image Url')
    jest.spyOn(axios, 'get').mockImplementation(() => {
      return Promise.reject(fetchFailError)
    })

    return fetchDogApiUrl('arbitrary.co').catch((err) => {
      expect(err).toEqual(fetchFailError)
    })
  })
})

describe('WriteDogBreedUrl:', () => {
  test('should write in file', () => {
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb()
    })
    return writeDogBreedUrl('something.txt', 'imageUrl.com').then((message) => {
      expect(message).toEqual('Successfully written in file')
    })
  })
  test('should throw an error on passing empty file path', () => {
    const emptyPathError = new Error('Cannot write Dog image url in given path')
    jest.spyOn(fs, 'writeFile').mockImplementation((_, __, cb) => {
      cb(emptyPathError)
    })
    return writeDogBreedUrl('', 'imageUrl.com').catch((err) => {
      expect(err).toEqual(emptyPathError)
    })
  })
})

describe('ReadDogBreed:', () => {
  test('should read in file', () => {
    const readFileSpy = jest
      .spyOn(fs, 'readFile')
      .mockImplementation((_, __, cb) => {
        cb()
      })
    return readDogBreed().then(() => {
      expect(readFileSpy).toHaveBeenCalledTimes(1)
    })
  })
  test('should throw an error in writing the image url', () => {
    const fileNotFoundError = new Error('File Not Found')
    jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb(fileNotFoundError)
    })
    return readDogBreed().catch((err) => {
      expect(err).toEqual(fileNotFoundError)
    })
  })
})

describe('FetchStoreImageUrl', () => {
  test('should return true  after writing dog image url', () => {
    return expect(fetchStoreImageUrl()).resolves.toBe(true)
  })

  test('should throw an error on file not found', () => {
    const fileNotFoundError = new Error('File Not Found')
    jest.spyOn(fs, 'readFile').mockImplementation((_, __, cb) => {
      cb(fileNotFoundError)
    })
    return fetchStoreImageUrl().catch((err) => {
      expect(err).toEqual(fileNotFoundError)
    })
  })
})
