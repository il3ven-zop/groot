const binarySearch = require('./problem2')

describe('Binary Search', () => {
  describe('test for valid inputs', () => {
    test.each([
      [[1, 2, 3, 4, 5], 6, -1],
      [[1, 2, 3, 4, 5], 4, 3],
      [[1, 2, 3, 4, 5], 1, 0]
    ])(
      'given %p and %p as arguments, returns %p',
      (firstArg, secondArg, expectedResult) => {
        const result = binarySearch(firstArg, secondArg)
        expect(result).toEqual(expectedResult)
      }
    )
  })

  describe('test for invalid inputs', () => {
    test('Should throw a error on invalid input', () => {
      const result1 = () => {
        binarySearch([2, 4, 5, 7], undefined)
      }
      const result2 = () => {
        binarySearch(undefined, 3)
      }
      expect(result1).toThrow('Recheck:Invalid input')
      expect(result2).toThrow('Recheck:Invalid input')
    })

    test('Should throw a error on non numerical array value', () => {
      const result1 = () => {
        binarySearch([1, 2, 'rishi'], 3)
      }
      expect(result1).toThrow('Array contains non numerical value')
    })
  })
})
