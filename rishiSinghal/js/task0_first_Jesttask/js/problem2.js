const binarySearch = (arr, value) => {
  if (!Array.isArray(arr) || isNaN(value)) {
    throw new Error('Recheck:Invalid input')
  }
  let leftIndex = 0
  let rightIndex = arr.length - 1
  while (leftIndex <= rightIndex) {
    const mid = Math.floor((leftIndex + rightIndex) / 2)
    if (isNaN(arr[mid])) {
      throw new Error('Array contains non numerical value')
    }

    if (arr[mid] === value) {
      return mid
    } else if (arr[mid] < value) {
      leftIndex = mid + 1
    } else {
      rightIndex = mid - 1
    }
  }
  return -1
}

module.exports = binarySearch
