const fizzBuzz = (input) => {
  if (isNaN(input)) {
    throw new Error('Non numerical input is passed')
  }
  if (input < 0) {
    throw new Error('Negative numbers not allowed')
  }

  if (input % 3 === 0 && input % 5 === 0) {
    return 'FizzBuzz'
  } else if (input % 3 === 0) {
    return 'Fizz'
  } else if (input % 5 === 0) {
    return 'Buzz'
  } else {
    return input
  }
}
module.exports = fizzBuzz
