const acmIcpcTeam = require('./problem3')

describe('Acm Icm', () => {
  describe('test for valid inputs', () => {
    test.each([
      [['11011', '00111', '01010'], 3, 5, [5, 1]],
      [['10101', '11100', '11010', '00101'], 4, 5, [5, 2]]
    ])(
      'given %p, %p and %p as arguments, returns %p',
      (firstArg, secondArg, thirdArg, expectedResult) => {
        const result = acmIcpcTeam(firstArg, secondArg, thirdArg)
        expect(result).toEqual(expectedResult)
      }
    )
  })

  describe('test for invalid inputs', () => {
    test('Should throw error on attendee for out of range', () => {
      const result = () => {
        acmIcpcTeam(['11011', '00111', '01010'], 1, 5)
      }
      expect(result).toThrowError('attendee are not in range 2 to 500')
    })

    test('Should throw error on topics for out of range', () => {
      const result = () => {
        acmIcpcTeam(['11011', '00111', '01010'], 5, 0)
      }
      expect(result).toThrowError('topics are not in range 1 to 500')
    })
  })
})
