const fs = require('fs')
const axios = require('axios')

const path = require('path')
const inputFile = 'dogs.txt'
const outputFile = 'dogsImageUrl.txt'

/**
 * Read data from the file .
 *
 * @param {string} inputFile Name of the input file.
 * @return {string} link Return link for making api call.
 */
const readData = (inputFile) => {
  const file = path.join(__dirname, inputFile)
  const readFilePromise = new Promise(function (resolve, reject) {
    fs.readFile(file, (err, data) => {
      if (err) reject(new Error('Invalid file passed as parameter'))

      resolve(data)
    })
  })
  return readFilePromise.then((response) => {
    const text = response.toString()
    const link = 'https://dog.ceo/api/breed/' + text + '/images/random'
    return link
  })
}
/**
 * Write Data into the file .
 *
 * @param {string} inputFile Name of the input file.
 * @param {string} outputFile Name of the output file.
 * @return {Promise} writeFilePromise Return writeFilePromise.
 */
const writingData = (finalOutput, outputFile) => {
  const writeFilePromise = new Promise(function (resolve, reject) {
    fs.writeFile(path.join(__dirname, outputFile), finalOutput, (error) => {
      if (error) reject(new Error('Could not write into the file'))

      resolve('Written Data into the file')
    })
  })
  return writeFilePromise
}
const getDogImages = async (link) => {
  try {
    const imageUrl = await Promise.all([
      axios.get(link),
      axios.get(link),
      axios.get(link),
      axios.get(link),
      axios.get(link)
    ])
    const imageUrls = imageUrl.map((element) => {
      return element.data.message
    })
    const finalOutput = imageUrls.join('\n')
    return finalOutput
  } catch {
    throw new Error('Failed to fetch images from API')
  }
}
const fetchingDogImages = async () => {
  const LINK = await readData(inputFile)
  const finalOutput = await getDogImages(LINK)
  await writingData(finalOutput, outputFile)
}
module.exports = {
  fetchingDogImages,
  writingData,
  readData,
  getDogImages
}
