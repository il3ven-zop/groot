const axios = require('axios')
const {
  fetchingDogImages,
  writingData,
  readData,
  getDogImages
} = require('./apiFetchingMultipleImages')
const path = require('path')
const fs = require('fs')

describe('Testing DogFetchApi for fetching multiple images', () => {
  test('should check if the axios get method is called 5 times', async () => {
    const spy = jest.spyOn(axios, 'get')
    await fetchingDogImages()
    expect(spy).toHaveBeenCalledTimes(5)
  })
  test('should throw error if data could not be written into the file', async () => {
    await expect(
      writingData('https://images.dog.ceo/breeds/boxer/n02108089_3162.jpg', '')
    ).rejects.toStrictEqual(new Error('Could not write into the file'))
  })
  test('should throw error if invalid input file is passed as parameter', async () => {
    await expect(readData('')).rejects.toStrictEqual(
      new Error('Invalid file passed as parameter')
    )
  })
  test('should check if input file is read successfully', async () => {
    const text = await readData('dogs.txt')
    expect(text).toBe('https://dog.ceo/api/breed/boxer/images/random')
  })
  test('should check if data was written in the file the file successfully', async () => {
    const text = await writingData(
      'https://images.dog.ceo/breeds/boxer/n02108089_3162.jpg',
      'testFile.txt'
    )
    expect(text).toBe('Written Data into the file')
    fs.unlinkSync(path.join(__dirname, 'testFile.txt'))
  })
  test('should throw error if fails to fetch images from API', async () => {
    await expect(
      getDogImages('https://dog.ceo/api/breed/polar/images/random')
    ).rejects.toStrictEqual(new Error('Failed to fetch images from API'))
  })
})
