function binarySearch(inputArray, key) {
  // Checking if the key is not valid
  if (isNaN(key)) {
    throw new Error('Key to find Not a number')
  }
  // checking if the array is not valid
  if (!Array.isArray(inputArray)) {
    throw new Error('Invalid Array Passes as Input')
  }
  // start index,end index declaration
  let end = inputArray.length - 1
  let start = 0
  // Value if element not found
  let finalIndex = -1
  while (start <= end) {
    const middle = Math.floor((start + end) / 2)
    if (key === inputArray[middle]) {
      finalIndex = middle
      return finalIndex
    } else if (key < inputArray[middle]) {
      end = middle - 1
    } else {
      start = middle + 1
    }
  }
  // return final index if element not found
  return finalIndex
}

module.exports = binarySearch
