const fs = require('fs')
const axios = require('axios')
const path = require('path')
const inputFile = 'dogs.txt'
const outputFile = 'apiFetchingLink.txt'

// function reading data from the input file and returning promise
const readDataFromFile = (inputFile) => {
  const file = path.join(__dirname, inputFile)
  const readFilePromise = new Promise(function (resolve, reject) {
    fs.readFile(file, (err, data) => {
      if (err) reject(new Error('The File does not exist'))

      resolve(data)
    })
  })
  return readFilePromise.then((response) => {
    const text = response.toString()
    const link = 'https://dog.ceo/api/breed/' + text + '/images/random'
    return link
  })
}

// function writing data into the file and returning a promise in return
const writingDataToFile = (apiFetching, outputFile) => {
  const imageUrl = apiFetching.message
  console.log(imageUrl)
  const writeFilePromise = new Promise(function (resolve, reject) {
    fs.writeFile(path.join(__dirname, outputFile), imageUrl, (error) => {
      if (error) reject(new Error('Could not write into the file'))

      resolve('Written Data into the file')
    })
  })
  return writeFilePromise
}

// function making api get request and returning promise in form of axios
const fetchingApi = (inputFile, outputFile) => {
  return readDataFromFile(inputFile).then((response) => {
    const link = response
    return axios
      .get(link)
      .then((apiFetching) => {
        return writingDataToFile(apiFetching.data, outputFile)
      })
      .then((response) => {
        console.log(response)
      })
      .catch(() => {
        throw new Error('Dog breed does not exist')
      })
  })
}
fetchingApi(inputFile, outputFile)
module.exports = { fetchingApi, writingDataToFile, readDataFromFile }
