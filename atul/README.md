Name - **Atul Rana**<br>
College - **JAYEPEE INSTITUTE OF INFORMATION TECHNOLOGY NOIDA**<br>
Hobbies - **Singing , Playing Guitar , Playing Cricket**<br>

JavaScript - **Task - 0**<br>

Description:<br>
Writing the code for the following listed programs and also did the unit testing<br>

Programs Question:<br>
1 - **Write Fizz buzz program and write the unit tests for the same.**<br>
2 - **Write Binary search program and write the unit tests for the same.**<br>
3 - **Solve the ACM ICPC team Problem and write the unit tests for the same.**<br>

JavaScript - **Task -1**<br>

Description:<br>
API End point: https://dog.ceo/dog-api/documentation/breed<br>

1 - **read the breed name from the .txt file eg: retriever**<br>
2 - **use Fetch or Axios to call your api and get the response**<br>
3 - **Get the image from the response and store it in a file**<br>
4 - **Write the unit tests for the task**<br>
