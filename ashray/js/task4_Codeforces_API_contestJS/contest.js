const fetch = require('node-fetch')
const { CODEFORCES_URL_CONTEST } = require('../constants')

const getTopHandles = async (contestID) => {
  try {
    const getURL =
      CODEFORCES_URL_CONTEST + '?contestId=' + contestID + '&from=1&count=10'
    const response = await (await fetch(getURL)).json()
    const codeforcesData = response.result.rows
    const filtered = codeforcesData.filter(
      (entry) => entry.party.members.length === 1
    )
    filtered.forEach((element, index, array) => {
      array[index] = element.party.members[0].handle
    })
    return filtered
  } catch (err) {
    return 'could not retrieve results'
  }
}

getTopHandles(566)

module.exports = getTopHandles
