/* istanbul ignore file */
// eslint-disable-next-line no-unused-vars
function check() {
  const fname = document.getElementById('f-name')
  const lname = document.getElementById('l-name')
  const email = document.getElementById('email')
  const password = document.getElementById('password')

  const efname = document.getElementById('ef-name')
  const elname = document.getElementById('el-name')
  const eemail = document.getElementById('eemail')
  const epassword = document.getElementById('epassword')

  const efnameicon = document.getElementById('ef-nameicon')
  const elnameicon = document.getElementById('el-nameicon')
  const eemailicon = document.getElementById('eemailicon')
  const epasswordicon = document.getElementById('epasswordicon')

  if (!fname?.value) {
    fname.classList.add('input-error')
    efnameicon.classList.add('error-show')
    efname.classList.add('error-show')
  } else {
    fname.classList.remove('input-error')
    efnameicon.classList.remove('error-show')
    efname.classList.remove('error-show')
  }

  if (!lname?.value) {
    elname.classList.add('error-show')
    elnameicon.classList.add('error-show')
    lname.classList.add('input-error')
  } else {
    elname.classList.remove('error-show')
    elnameicon.classList.remove('error-show')
    lname.classList.remove('input-error')
  }

  if (!checkEmail(email?.value)) {
    eemail.classList.add('error-show')
    eemailicon.classList.add('error-show')
    email.classList.add('input-error')
  } else {
    eemail.classList.remove('error-show')
    eemailicon.classList.remove('error-show')
    email.classList.remove('input-error')
  }

  if (!password?.value) {
    epassword.classList.add('error-show')
    epasswordicon.classList.add('error-show')
    password.classList.add('input-error')
  } else {
    epassword.classList.remove('error-show')
    epasswordicon.classList.remove('error-show')
    password.classList.remove('input-error')
  }
}

function checkEmail(email) {
  const emailCheckRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return emailCheckRegex.test(String(email).toLowerCase())
}
