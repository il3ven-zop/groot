const { getRating, maxRating } = require('./user')

describe('getRating()', () => {
  test('higest rated user getRating() test-1', async () => {
    const handleArray = [
      'lokpati',
      'Ashishgup',
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await getRating(handleArray)
    expect(response).toStrictEqual({ highestRated: 'Ashishgup' })
  })
  test('could not retrieve results getRating test-2', async () => {
    const handleArray = [
      'lokpati',
      undefined,
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await getRating(handleArray)
    expect(response).toStrictEqual('could not retrieve results')
  })
  test('could not retrieve results getRating test-3', async () => {
    const handleArray = 'dummyString'
    const response = await getRating(handleArray)
    expect(response).toStrictEqual('could not retrieve results')
  })
})

describe('maxRating()', () => {
  test('highest rating ever maxRating() test-1', async () => {
    const handleArray = [
      'lokpati',
      'Ashishgup',
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await maxRating(handleArray)
    expect(response).toStrictEqual({ maxRated: 'T1duS' })
  })
  test('could not retrieve results maxRating() test-2', async () => {
    const handleArray = [
      'lokpati',
      undefined,
      'T1duS',
      'vivo',
      'Anushi1998',
      'Kruti_20',
      'CMin2021'
    ]
    const response = await maxRating(handleArray)
    expect(response).toStrictEqual('could not retrieve results')
  })
  test('could not retrieve results maxRating() test-3', async () => {
    const handleArray = 'dummyString'
    const response = await maxRating(handleArray)
    expect(response).toStrictEqual('could not retrieve results')
  })
})
