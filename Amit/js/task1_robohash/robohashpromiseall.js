const getImage = require('./robohash.js')

const booleanFunction = (element) => element === true

function robohashPromise() {
  return Promise.all([
    getImage(1),
    getImage(2),
    getImage(3),
    getImage(4)
  ]).then((value) => value.every(booleanFunction))
}

robohashPromise()
module.exports = robohashPromise
