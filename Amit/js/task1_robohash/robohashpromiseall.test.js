const robohashPromise = require('./robohashpromiseall')

// TODO: why we are not spying on function depencdency
describe('robohash promise all testing', () => {
  test('Image is successfully uploaded', async () => {
    const result = await robohashPromise()
    expect(result).toBeTruthy()
  })
})
