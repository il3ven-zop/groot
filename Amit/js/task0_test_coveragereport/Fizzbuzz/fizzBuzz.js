function fizzBuzz(j) {
  if (isNaN(j)) {
    throw new Error('Invalid Input')
  }
  if (j < 1 || j > 100) {
    throw new Error('Invalid Input')
  }
  if (j % 15 === 0) {
    return 'FizzBuzz'
  } else if (j % 5 === 0) {
    return 'Buzz'
  } else if (j % 3 === 0) {
    return 'Fizz'
  } else {
    return j
  }
}
module.exports = fizzBuzz
