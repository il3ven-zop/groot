function binarySearch(leftIndex, rightIndex, numbertosearch, array) {
  if (!Array.isArray(array) || isNaN(numbertosearch)) {
    throw new Error('Invalid Input')
  }
  while (leftIndex <= rightIndex) {
    const mid = leftIndex + Math.floor((rightIndex - leftIndex) / 2)
    if (array[mid] === numbertosearch) return mid
    else if (array[mid] > numbertosearch) rightIndex = mid - 1
    else leftIndex = mid + 1
  }
  return -1
}

module.exports = binarySearch
