/* istanbul ignore file */
/* eslint-disable no-unused-vars */

const passContain = '**Password must be 6 characters long'
const passUpperError = '**Password must contains a Capital letter'
const passDigitError = '**Password must contains a numeric value'
const passSpecialError = '**Password must contains a special character'

// name validation
const nameValidation = () => {
  const name = document.register.name.value
  const nameMsg = document.getElementById('name__msg')

  if (!name) nameMsg.innerHTML = '**Name required'
  else nameMsg.innerHTML = ''
}

function emailRegexValidation(email) {
  const regexEmail = /([a-z\d.-]+)@([a-z\d-]+)\.([a-z]{2,8})/
  return regexEmail.test(email)
}

// email validation and inside this used regex function for email
const emailValidation = () => {
  const email = document.register.email.value
  const emailMsg = document.getElementById('email__msg')

  if (email.length > 0) {
    if (emailRegexValidation(email)) emailMsg.innerHTML = ''
    else emailMsg.innerHTML = '**Invalid Email'
  } else {
    emailMsg.innerHTML = '**Email required'
  }
}

// password upper case validation
function checkUpperCase(password) {
  const regexUpperPassword = /(?=.*[A-Z])/
  return regexUpperPassword.test(password)
}

// password digit validation
function checkDigit(password) {
  const regexDigit = /.*\d.*/
  return regexDigit.test(password)
}

// password special character validation
function checkSpecialCharacter(password) {
  const regexSpecialChar = /(?=.*[-+_!@#$%^&*., ?])/
  return regexSpecialChar.test(password)
}

// main function of password validation for register
const passwordValidationRegister = () => {
  const password = document.register.password.value
  const passwordMsg = document.getElementById('password__msg')

  if (password) {
    if (password.length < 6) {
      passwordMsg.innerHTML = passContain
    } else {
      if (!checkUpperCase(password)) passwordMsg.innerHTML = passUpperError
      else {
        if (checkDigit(password) === false)
          passwordMsg.innerHTML = passDigitError
        else {
          if (checkSpecialCharacter(password) === false)
            passwordMsg.innerHTML = passSpecialError
          else passwordMsg.innerHTML = ''
        }
      }
    }
  } else {
    passwordMsg.innerHTML = '**Password required'
  }
}

// confirm password matching with password validation
const confirmPassValidation = () => {
  const password = document.register.password.value
  const confirmpassword = document.register.confirmpassword.value
  const confirmpasswordMsg = document.getElementById('confirmpassword__msg')

  if (confirmpassword.length === 0) {
    confirmpasswordMsg.innerHTML = '**Confirm Password is required'
  } else if (password !== confirmpassword) {
    confirmpasswordMsg.innerHTML = '**Password and Confirm Password not matched'
  } else confirmpasswordMsg.innerHTML = ''
}

// on click of register button validation of all the fields are filled or not
function validateRegister() {
  const name = document.register.name.value
  const email = document.register.email.value
  const password = document.register.password.value
  const confirmpassword = document.register.confirmpassword.value

  const nameMsg = document.getElementById('name__msg')
  const emailMsg = document.getElementById('email__msg')
  const passwordMsg = document.getElementById('password__msg')
  const confirmpasswordMsg = document.getElementById('confirmpassword__msg')

  // name is required
  if (!name) nameMsg.innerHTML = '**Name required'

  // email is required
  if (!email) emailMsg.innerHTML = '**Email required'

  // password validation
  if (!password) passwordMsg.innerHTML = '**Password required'

  // confirm password validation
  if (!confirmpassword)
    confirmpasswordMsg.innerHTML = '**Confirm Password required'
}
