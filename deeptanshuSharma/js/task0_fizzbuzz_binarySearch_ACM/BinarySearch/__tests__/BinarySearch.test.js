const BinarySearch = require('../BinarySearch.js')

describe('Binary Search Tests', () => {
  const cases = [
    [[[2, 4, 5, 7, 8, 10, 11, 23], 5], 2],
    [[[2, 4, 5, 7, 8, 10, 11, 23], 10], 5],
    [[[2, 4, 5, 7, 8, 10, 11, 23], 11], 6]
  ]
  test.each(cases)(
    'should return the correct index and search at right part of the array',
    (input, output) => {
      const result = BinarySearch(input[0], input[1])
      expect(result).toEqual(output)
    }
  )

  test('should return -1 if key is not found', () => {
    const input = [[2, 4, 5, 7, 8, 10, 11, 23], 3]

    const output = -1

    expect(BinarySearch(input[0], input[1])).toEqual(output)
  })

  test('should throw an error - empty array', () => {
    const input = () => {
      BinarySearch([], 3)
    }

    const output = 'empty array'

    expect(input).toThrow(output)
  })

  test('should throw an error - not an array', () => {
    const input = () => {
      BinarySearch('arr', 3)
    }

    const output = 'not an array'

    expect(input).toThrow(output)
  })
})
