function BinarySearch(arr, item) {
  if (!Array.isArray(arr)) {
    throw new Error('not an array')
  }

  if (arr.length === 0) {
    throw new Error('empty array')
  }

  let l = 0
  let r = arr.length - 1

  while (l <= r) {
    const m = Math.floor(l + (r - l) / 2)

    if (arr[m] === item) return m

    if (arr[m] < item) l = m + 1
    else r = m - 1
  }

  return -1
}

module.exports = BinarySearch
