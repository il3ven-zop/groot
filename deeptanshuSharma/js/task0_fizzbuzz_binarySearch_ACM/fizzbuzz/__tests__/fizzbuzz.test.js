const fizzBuzz = require('../index.js')

describe('FizzBuzz Tests', () => {
  const cases = [
    [
      () => {
        fizzBuzz(-3)
      },
      'Invalid Input'
    ],
    [
      () => {
        fizzBuzz(1.34)
      },
      'Invalid Input'
    ],
    [
      () => {
        fizzBuzz(0)
      },
      'Invalid Input'
    ],
    [
      () => {
        fizzBuzz('abc')
      },
      'Invalid Input'
    ]
  ]
  test.each(cases)(
    'should throw an error invalid input on negative integers or non integer input',
    (input, output) => {
      expect(input).toThrow(output)
    }
  )

  const successCases = [
    [5, ['1', '2', 'Fizz', '4', 'Buzz']],
    [3, ['1', '2', 'Fizz']],
    [
      15,
      [
        '1',
        '2',
        'Fizz',
        '4',
        'Buzz',
        'Fizz',
        '7',
        '8',
        'Fizz',
        'Buzz',
        '11',
        'Fizz',
        '13',
        '14',
        'FizzBuzz'
      ]
    ]
  ]
  test.each(successCases)(
    'should return fizz if the number is multiple of 3, buzz if number is multiple of 5 and return number itself if the number is not multiple of 3 and 5',
    (input, output) => {
      const result = fizzBuzz(input)
      expect(result).toEqual(output)
    }
  )
})
