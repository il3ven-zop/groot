const acm = require('../ACM')

describe('ACM tests', () => {
  const cases = [
    [
      () => {
        acm([])
      },
      'at least 2 teams required in input'
    ],
    [
      () => {
        acm(['10010'])
      },
      'at least 2 teams required in input'
    ],
    [
      () => {
        acm('test')
      },
      'not an array'
    ]
  ]
  test.each(cases)(
    'should throw an error when an input of less than 2 elements is given or a non array element given',
    (input, output) => {
      expect(input).toThrow(output)
    }
  )

  const successCases = [
    [
      ['10101', '11110', '00010'],
      [5, 1]
    ],
    [
      ['10101', '11100', '11010', '00101'],
      [5, 2]
    ]
  ]
  test.each(successCases)(
    'Should return correct output for the given input',
    (input, output) => {
      const result = acm(input)
      expect(result).toEqual(output)
    }
  )
})
