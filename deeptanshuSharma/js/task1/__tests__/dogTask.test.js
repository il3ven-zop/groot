const fetchDog = require('../dogTask.js')
const axios = require('axios')
const fs = require('fs')

jest.mock('axios')
jest.mock('fs')

describe('Task 1 tests. Tests fetchDog method', () => {
  beforeEach(() => {
    fs.readFile.mockImplementation((fileName, callBack) =>
      callBack(undefined, 'retriever')
    )
    fs.writeFile.mockImplementation((fileName, text, callBack) =>
      callBack(undefined)
    )

    const resp = {
      data: {
        message: [
          'https://images.dog.ceo/breeds/retriever-chesapeake/n02099849_1068.jpg'
        ]
      }
    }

    axios.get.mockResolvedValue(resp)
  })

  test('should save the image url received from the API successfully', () => {
    return fetchDog('breed.txt').then((data) => {
      expect(data).toBe('Saved!')
    })
  })

  test('Should return an error showing the API is Down', () => {
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error('API down'))
    )

    return fetchDog('breed.txt').catch((error) => {
      expect(error.toString()).toBe('Error: API down')
    })
  })

  test('should return an error if the specified file does not exist', () => {
    fs.readFile.mockImplementationOnce((fileName, callBack) =>
      callBack(new Error('Error in reading file'))
    )

    return fetchDog('br.txt').catch((error) => {
      expect(error.toString()).toBe('Error: Error in reading file')
    })
  })

  test('should return an error if cannot write to a file successfully', () => {
    fs.writeFile.mockImplementationOnce((fileName, text, callBack) =>
      callBack(new Error('Write file error'))
    )

    return fetchDog('breed.txt').catch((error) => {
      expect(error.toString()).toBe('Error: Write file error')
    })
  })
})
