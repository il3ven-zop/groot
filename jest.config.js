module.exports = {
  //  This will collect coverage information from the all the file under src that has .ts extension
  //  excluding the src/types and exclude coverage from node_modules and dist folder
  collectCoverageFrom: [
    '**/*.js?(x)',
    '!**/jest.config.js',
    '!**/index.js',
    '!**/node_modules/**',
    '!**/coverage/**',
    '!scripts/**',
    '!**/dist/**',
    '!linter.js'
  ],

  // coverageReporter of type "text" is used to see a coverage summary in the console output.
  // json options creates the coverage directory if it is not present and
  // dump the report in form of json object for each test file i.e coverage/coverage-final.json
  // lcov creates the coverage/lcov-report folder and dump data in html
  // follow the link for valid coverageReporters https://github.com/istanbuljs/istanbuljs/tree/master/packages/istanbul-reports/lib

  // TODO : update coverage as a part of CI process
  coverageReporters: ['text', 'json', 'lcov'],

  // TODO: increase the coverage threshold value after we are finished with the test case
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },

  // Allows for a label to be printed alongside a test while it is running
  displayName: 'ci',

  // An array of file extensions your modules use. If you require modules without specifying a
  // file extension, these are the extensions Jest will look for, in left-to-right order.
  moduleFileExtensions: ['js', 'json', 'jsx'],

  // Automatically reset mock state before every test.
  resetMocks: true,

  // By default, each test file gets its own independent module registry. Enabling resetModules
  // goes a step further and resets the module registry before running each individual test.
  resetModules: true,

  // it will set node like environment for testing purpose ans overrides the default environment
  testEnvironment: 'node',

  // test match says test file must contain .test.ts extension, add if you want some other extension.
  testMatch: ['**/*.test.js?(x)'],

  // override the default timeout value i.e 5000, if test is failing because of timeout
  // this value could be increased, if testTimeout is the only reason for timeout
  testTimeout: 10000,

  // enables the test description to be printed just after the test suite, if there are multiple
  // test suite
  verbose: true
}
