/*
Input: array of string
Return : size of maximum known subject in team of two with how many teams of same size.
*/

function acmTeam(topic) {
  if (!Array.isArray(topic)) {
    throw new Error('Invalid Input')
  }
  const totalStudent = topic.length
  if (totalStudent < 2) {
    throw new Error('Size should be atlist 2!')
  }
  const totalSubject = topic[0].length
  if (totalSubject < 1) {
    throw new Error('Subject should be atleast 1!')
  }
  let maximumKnownSubject = 0
  let totalTeam = 1
  for (let i = 0; i < totalStudent - 1; i++) {
    for (let j = i + 1; j < totalStudent; j++) {
      let cnt = 0
      for (let k = 0; k < totalSubject; k++) {
        if (topic[i][k] === '1' || topic[j][k] === '1') {
          cnt++
        }
      }
      if (cnt > maximumKnownSubject) {
        maximumKnownSubject = cnt
        totalTeam = 1
      } else if (cnt === maximumKnownSubject) {
        totalTeam++
      }
    }
  }
  return [maximumKnownSubject, totalTeam]
}

module.exports = acmTeam
