const acmTeam = require('./acm_Icpc')

describe('ACMTeam', () => {
  test('Should throw error when input is not array of string', () => {
    const result = () => acmTeam(undefined)
    expect(result).toThrow('Invalid Input')
  })
  test('Throw Error error when size array is less than 2', () => {
    const result = () => acmTeam(['11100'])
    expect(result).toThrow('Size should be atlist 2!')
  })
  test('Should throw error when string is empty', () => {
    const result = () => acmTeam(['', ''])
    expect(result).toThrow('Subject should be atleast 1!')
  })
  test('shold return [5,3]', () => {
    const result = acmTeam(['11100', '11111', '11111'])
    expect(result).toEqual([5, 3])
  })
  test('shold return [5,1]', () => {
    const result = acmTeam(['10101', '11110', '00010'])
    expect(result).toEqual([5, 1])
  })
})
