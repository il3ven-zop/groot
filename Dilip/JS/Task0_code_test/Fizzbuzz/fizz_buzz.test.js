const fizzBuzz = require('./fizz_buzz')
describe('FizzBuzz', () => {
  test.each([-2, 105, 'Dilip'])(
    'Should throw Error when Input is not valid',
    (a) => {
      const result = () => fizzBuzz(a)
      expect(result).toThrow('Invalid Input')
    }
  )
  test('should print FizzBuzz when input is multiple of 5 & 3 both', () => {
    expect(fizzBuzz(15)).toEqual('FizzBuzz')
  })
  test('should print Fizz when input is multiple of 3', () => {
    expect(fizzBuzz(3)).toEqual('Fizz')
  })
  test('should print Buzz when input is multiple of 5', () => {
    expect(fizzBuzz(5)).toEqual('Buzz')
  })
  test('should print same number when input is neigther multiple of 5 nor multiple of 3', () => {
    expect(fizzBuzz(1)).toEqual(1)
  })
})
