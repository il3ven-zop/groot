function fizzBuzz(i) {
  if (isNaN(i) || i < 1 || i > 100) {
    throw new Error('Invalid Input')
  }
  if (i % 15 === 0) {
    return 'FizzBuzz'
  } else if (i % 5 === 0) {
    return 'Buzz'
  } else if (i % 3 === 0) {
    return 'Fizz'
  } else {
    return i
  }
}
module.exports = fizzBuzz
