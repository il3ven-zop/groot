/* istanbul ignore file */

const emailErrorMessage = '* input type must be email'
const passwordErrorLower = '* must contain lowercase letter'
const passwordErrorUpper = '* must contain Uppercase letter'
const passwordErrorDigit = '* must contain a digit'
const passwordErrorLength = '* Length should be atleast 8'
const confirmPasswordError = '* should be same as password'
const nameErrorMessage = '* this field must be non empty'
const regExpEmail = /\S+@\S+\.\S+/
const lowerCaseLetters = /[a-z]/g
const upperCaseLetters = /[A-Z]/g
const numbers = /[0-9]/g

let isEmailValid = false
let isPasswordValid = false
let isConfirmPass = false
let isName = false

function displayEmailError() {
  const emailError = document.getElementById('email-error')
  emailError.innerHTML = isEmailValid ? '' : emailErrorMessage
}

function displayPasswordErrorUpper() {
  const passError = document.getElementById('password-error')
  passError.innerHTML = isPasswordValid ? '' : passwordErrorUpper
}

function displayPasswordErrorLower() {
  const passError = document.getElementById('password-error')
  passError.innerHTML = isPasswordValid ? '' : passwordErrorLower
}

function displayPasswordErrorNumber() {
  const passError = document.getElementById('password-error')
  passError.innerHTML = isPasswordValid ? '' : passwordErrorDigit
}
function displayPasswordErrorLength() {
  const passError = document.getElementById('password-error')
  passError.innerHTML = isPasswordValid ? '' : passwordErrorLength
}

function displayConfirmError() {
  const passConfirm = document.getElementById('confirm-error')
  passConfirm.innerHTML = isConfirmPass ? '' : confirmPasswordError
}

function displayNameError() {
  const nameError = document.getElementById('name-error')
  nameError.innerHTML = isName ? '' : nameErrorMessage
}

// validateName() is not invoked from this file so for ignoring esLint error I add below line
// eslint-disable-next-line no-unused-vars
function validateName(event) {
  const getName = event.value
  if (getName !== '') {
    makeValidClass(event)
    isName = true
    displayNameError()
  } else {
    makeInvalidClass(event)
    isName = false
    displayNameError()
  }
  toggleLoginButton()
}

// validateConfirmPassword() is not invoked from this file so for ignoring esLint error I add below line
// eslint-disable-next-line no-unused-vars
function validateConfirmPassword(event) {
  const pass1 = document.getElementById('password').value
  const pass2 = event.value
  if (pass1 === pass2) {
    makeValidClass(event)
    isConfirmPass = true
    displayConfirmError()
  } else {
    makeInvalidClass(event)
    isConfirmPass = false
    displayConfirmError()
  }
  toggleLoginButton()
}

// validateEmail() is not invoked from this file so for ignoring esLint error I add below line
// eslint-disable-next-line no-unused-vars
function validateEmail(event) {
  const getEmail = event.value
  if (getEmail !== '' && getEmail.match(regExpEmail)) {
    makeValidClass(event)
    isEmailValid = true
    displayEmailError()
  } else {
    makeInvalidClass(event)
    isEmailValid = false
    displayEmailError()
  }
  toggleLoginButton()
}

// validatePassword() is not invoked from this file so for ignoring esLint error I add below line
// eslint-disable-next-line no-unused-vars
function validatePassword(event) {
  const getPassword = event.value

  if (
    getPassword.match(lowerCaseLetters) &&
    getPassword.length >= 8 &&
    getPassword.match(upperCaseLetters) &&
    getPassword.match(numbers)
  ) {
    makeValidClass(event)
    isPasswordValid = true
    displayPasswordErrorUpper()
  } else if (!getPassword.match(upperCaseLetters)) {
    makeInvalidClass(event)
    isPasswordValid = false
    displayPasswordErrorUpper()
  } else if (!getPassword.match(lowerCaseLetters)) {
    makeInvalidClass(event)
    isPasswordValid = false
    displayPasswordErrorLower()
  } else if (!getPassword.match(numbers)) {
    makeInvalidClass(event)
    isPasswordValid = false
    displayPasswordErrorNumber()
  } else if (getPassword.length < 8) {
    makeInvalidClass(event)
    isPasswordValid = false
    displayPasswordErrorLength()
  }
  toggleLoginButton()
}

// saveInfo() is not invoked from this file so for ignoring esLint error I add below line
// eslint-disable-next-line no-unused-vars
function saveInfo() {
  const name = document.signupform.name.value
  const email = document.signupform.email.value
  alert(`your name is ${name} and email is ${email}`)
  window.location.href = 'login.html'
}

// validateName() is not invoked from this file so for ignoring esLint error I add below line
// eslint-disable-next-line no-unused-vars
function successFunction() {
  const email = document.loginform.email.value
  alert(`your email is ${email}`)
  window.location.href = 'login.html'
}

function makeValidClass(event) {
  event.classList.remove('is-invalid')
  event.classList.add('is-valid')
}

function makeInvalidClass(event) {
  event.classList.remove('is-valid')
  event.classList.add('is-invalid')
}

function toggleLoginButton() {
  const btnSignup = document.getElementById('btnSignup')
  if (btnSignup !== null) {
    btnSignup.disabled = !(
      isEmailValid &&
      isPasswordValid &&
      isConfirmPass &&
      isName
    )
  }
  const btnLogin = document.getElementById('btnLogin')
  if (btnLogin !== null) {
    btnLogin.disabled = !isEmailValid
  }
}
