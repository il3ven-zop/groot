const myRobot = require('./robohashAsyncAwait')
const allImageOccur = (elem) => elem

async function generateRobots() {
  const imageStatus = await Promise.all([
    myRobot(0),
    myRobot(1),
    myRobot(2),
    myRobot(4),
    myRobot(5)
  ])
  return imageStatus.every(allImageOccur)
}
generateRobots()
module.exports = generateRobots
