/*
This code will generate a Random Image of robot and put it to folder
named robohashImages with name 'robot0.jpg'
Here I used Async Await method.
*/
const axios = require('axios')
const fs = require('fs')
const myURL = 'https://robohash.org/'
function generateString() {
  return new Promise((resolve, reject) => {
    const generatedRandomString = Math.random().toString(36).substring(7)
    resolve(generatedRandomString)
  })
}
function callAPI(myURL) {
  return axios.get(myURL, { responseType: 'arraybuffer' })
}

function WriteToFile(image, index) {
  return new Promise((resolve, reject) => {
    const loc = './'
    fs.writeFile(loc + 'robot' + index + '.jpg', image.data, () => {
      resolve(true)
    })
  })
}
async function myRobot(index) {
  const str = await generateString()
  const image = await callAPI(myURL + str)
  const writeImage = await WriteToFile(image, index)
  return writeImage
}
myRobot(0)
module.exports = myRobot
