Name - **Dilip Valiya**

College - **Dhirubhai ambani institute of information and communication technology, Gandhinagar**

Hobbies - **cricket, reading gujarati books**

# Js Tasks

**Task1** - Testing code using jest
Done testing using jest for given three problems:

1.[acmIcpcTeam](https://www.hackerrank.com/challenges/acm-icpc-team/problem)
2.Binary search 3.[Fizzbuzz](https://www.geeksforgeeks.org/fizz-buzz-implementation/)

**Task2** - Robohash
Done api call to a server using axios which will give me data which contains image.
Here I use promise for doing that (Asynchronous process)

**Task3** - Login & Register page
create login and register page using html, css and javascript

Here is Gif for signUp page

![Alt Text](./JS/Task3LoginSignUp/GIF/signUp.gif)

Here is Gif for login page

![Alt Text](./JS/Task3LoginSignUp/GIF/login.gif)
